"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(my_list: list, my_sep: str):

    if type(my_list) != list:
        return 'First arg must be list!'
    if type(my_sep) != str:
        return 'Second arg must be str!'
    if len (my_list) == 0:
        return ()

    the_string = my_sep.join(str(elem) for elem in my_list)
    return the_string, the_string.count(my_sep) ** 2


if __name__ == '__main__':
    print(list_to_str(["apple", "pear", "lemon", "dog", "lemon"], "&"))

