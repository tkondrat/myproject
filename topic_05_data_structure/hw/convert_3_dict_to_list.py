"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if not my_dict:
        return [], [], 0, 0

    keys_list = list(my_dict.keys())
    values_list = list(my_dict.values())
    len_unique_keys = len(set(keys_list))
    len_unique_values = len(set(values_list))

    return keys_list, values_list, len_unique_keys, len_unique_values


if __name__ == '__main__':
    print(dict_to_list({1: "ойой", 2: "айай", 3: "опять", 4: "страдать"}))
