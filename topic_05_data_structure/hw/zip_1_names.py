"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(first_names: list, last_names: set):
    if type(first_names) != list:
        return 'First arg must be list!'
    if type(last_names) != set:
        return 'Second arg must be set!'
    if not first_names:
        return 'Empty list!'
    if not last_names:
        return 'Empty set!'

    return list(zip(first_names, last_names))


if __name__ == '__main__':

    print(zip_names(["Misha", "Slava", "Sasha", "Cheburasha"], {"Ivanov", "Sidorov", "Kozlov", "Nichej"}))

