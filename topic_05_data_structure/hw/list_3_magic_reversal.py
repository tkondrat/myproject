"""
Функция magic_reversal.

Принимает 1 аргумент: список my_list.

Создает новый список new_list (копия my_list), порядок которого обратный my_list.

Возвращает список, который состоит из
[второго элемента (индекс=1) new_list]
+ [предпоследнего элемента new_list]
+ [весь new_list].

Пример:
входной список [1,  'aa', 99]
new_list [99, 'aa', 1]
результат ['aa', 'aa', 99, 'aa', 1].

Если список состоит из одного элемента, то "предпоследний" = единственный и "второй элемент" = единственный.

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.

ВНИМАНИЕ: Изначальный список не должен быть изменен!
"""


def magic_reversal(my_list: list):
    if type(my_list) != list:
        return 'Must be list!'
    if not my_list:
        return 'Empty list!'
    if len(my_list) == 1:
        return my_list * 3

    new_list = my_list.copy()
    new_list.reverse()

    result = []
    el1 = new_list[1]
    el2 = new_list[-2]
    el3 = new_list

    result.append(el1)
    result.append(el2)
    result.extend(el3)

    return result


if __name__ == '__main__':
    print(magic_reversal([5]))
