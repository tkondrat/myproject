"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(ru_dict: dict, word: str):
    if type(ru_dict) != dict:
        return "Dictionary must be dict!"
    elif type(word) != str:
        return "Word must be str!"
    elif not ru_dict:
        return "Dictionary is empty!"
    elif not word:
        return "Word is empty!"

    ru_variants = []
    for ru_word, eng_words in ru_dict.items():
        if word in eng_words:
            ru_variants.append(ru_word)
    return ru_variants if len(ru_variants) > 0 else f"Can't find English word: {word}"


if __name__ == '__main__':
    test_dict = {"окей": ["okay", "aha", "yep"]}
    print(get_words_by_translation(test_dict, "aha"))


