"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(cars: list, prod_years: list):
    if type(cars) != list or type(prod_years) != list:
        return 'Must be list!'
    if not prod_years or not cars:
        return 'Empty list!'
    return list(zip_longest(cars, prod_years, fillvalue='???'))


if __name__ == '__main__':
    print(zip_car_year(["Ford", "Mercedes", "Volvo"], ["2003", "1985", '1900']))
