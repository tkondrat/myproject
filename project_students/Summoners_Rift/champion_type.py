from enum import Enum, auto


class ChampionType(Enum):
    """
    Information from https://ru.leagueoflegends.com/ru-ru/champions/
    """
    Маг = auto()
    Стрелок = auto()
    Танк = auto()

    @classmethod
    def min_value(cls):
        return cls.Маг.value

    @classmethod
    def max_value(cls):
        return cls.Танк.value
