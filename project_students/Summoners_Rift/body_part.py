from enum import Enum, auto


class BodyPart(Enum):
    Бездействие = auto()
    Сердце = auto()
    Разум = auto()
    Голова = auto()

    @classmethod
    def min_value(cls):
        return cls.Бездействие.value

    @classmethod
    def max_value(cls):
        return cls.Голова.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_
