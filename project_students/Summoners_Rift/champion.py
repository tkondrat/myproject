from project_students.Summoners_Rift.champion_type import ChampionType
from project_students.Summoners_Rift.champion_types_weaknesses import champion_defence_weaknesses_by_type as \
    weaknesses_by_type
from project_students.Summoners_Rift.body_part import BodyPart
from project_students.Summoners_Rift.champion_state import ChampionState


class Champion:
    def __init__(self,
                 name: str,
                 champion_type: ChampionType):
        self.name = name
        self.champion_type = champion_type
        self.weaknesses = weaknesses_by_type.get(champion_type, tuple())
        self.hp = 100
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20
        self.state = ChampionState.READY

    def __str__(self):
        return f"Чемпион: {self.name} | Роль: {self.champion_type.name}\nЗдоровье: {self.hp}"

    def next_step_points(self,
                         next_attack: BodyPart,
                         next_defence: BodyPart):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_point: BodyPart,
                opponent_hit_power: int,
                opponent_type: ChampionType):
        if opponent_attack_point == BodyPart.Бездействие:
            return "Мазила! Похоже, я опять в бронзовом дивизионе... "
        elif self.defence_point == opponent_attack_point:
            return "Поиграй лучше в Майнкрафт, мазила!"
        else:
            self.hp -= opponent_hit_power * (2 if opponent_type in self.weaknesses else 1)

            if self.hp <= 0:
                self.state = ChampionState.DEFEATED
                return "На этот раз ты оказался сильнее..."
            else:
                return "Ай, больно в ноге и вообще везде..."


if __name__ == '__main__':
    champ = Champion('Треш', ChampionType.Танк)
    print(champ)
