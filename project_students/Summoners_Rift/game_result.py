from enum import Enum, auto


class GameResult(Enum):
    Победы = auto()
    Поражения = auto()
    Ничья = auto()
