from project_students.Summoners_Rift.champion_type import ChampionType

champion_defence_weaknesses_by_type = {

    ChampionType.Маг: (ChampionType.Стрелок,
                       ChampionType.Танк),

    ChampionType.Стрелок: (ChampionType.Танк,
                           ChampionType.Маг),

    ChampionType.Танк: (ChampionType.Стрелок,)
}
