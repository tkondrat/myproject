import json

import telebot
from telebot import types
from telebot.types import Message

from project_students.Summoners_Rift.body_part import BodyPart
from project_students.Summoners_Rift.game_result import GameResult
from project_students.Summoners_Rift.champion import Champion
from project_students.Summoners_Rift.champion_npc import ChampionNPC
from project_students.Summoners_Rift.champion_by_type import champion_by_type
from project_students.Summoners_Rift.champion_state import ChampionState
from project_students.Summoners_Rift.champion_type import ChampionType

with open("./bot_token", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

state = {}

statistics = {}

stat_file = "game_stat.json"

body_part_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                               one_time_keyboard=True,
                                               row_width=len(BodyPart))

body_part_keyboard.row(*[types.KeyboardButton(body_part.name) for body_part in BodyPart])


def update_save_stat(chat_id, result: GameResult):
    print("Обновляю статистику", end="...")
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.Победы:
        statistics[chat_id]['Победы'] = statistics[chat_id].get('Победы', 0) + 1
    elif result == GameResult.Поражения:
        statistics[chat_id]['Поражения'] = statistics[chat_id].get('Поражения', 0) + 1
    elif result == GameResult.Ничья:
        statistics[chat_id]['Ничья'] = statistics[chat_id].get('Ничья', 0) + 1
    else:
        print(f"Такого результата {result} нет.")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("Готово!")


def load_stat():
    print('Загружаю статистику...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('Загрузка завершена!')
    except FileNotFoundError:
        statistics = {}
        print('Не могу найти такой файл :(')


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id,
                     "Приветствую тебя, призыватель!\n/start для начала игры\n/stat для отображения статистики")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Ты еще ни разу не играл!"
    else:
        user_stat = "Вот твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,

                     text="Готов сразиться в Ущелье Призывателей?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'ГЛ ХФ!')

        create_npc(message)

        ask_user_about_champion_type(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Набирайся сил и возвращайся!')
    else:
        bot.send_message(message.from_user.id,
                         'Ничего не понятно...')


def create_npc(message):
    print(f"Начинаю создавать NPC для chat id = {message.chat.id}.")
    global state
    champion_npc = ChampionNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_champion'] = champion_npc

    npc_picture_filename = champion_by_type[champion_npc.champion_type][champion_npc.name]
    bot.send_message(message.chat.id, 'Твой противник:')
    with open(f"../pictures/{npc_picture_filename}", 'rb') as file:
        bot.send_document(message.chat.id, file)
    bot.send_message(message.chat.id, champion_npc)

    print(f"Создание NPC для chat id = {message.chat.id} завершено.")


def ask_user_about_champion_type(message):
    markup = types.InlineKeyboardMarkup()

    for champion_type in ChampionType:
        markup.add(types.InlineKeyboardButton(text=champion_type.name,
                                              callback_data=f"champion_type_{champion_type.value}"))

    bot.send_message(message.chat.id, "Выбери роль:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "champion_type_" in call.data)
def champion_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Ойой, что-то не так! Перезапусти сессию, плиз!")
    else:
        champion_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, "Выбери чемпиона:")

        ask_user_about_champion_by_type(champion_type_id, call.message)


def ask_user_about_champion_by_type(champion_type_id, message):
    champion_type = ChampionType(champion_type_id)
    champion_dict_by_type = champion_by_type.get(champion_type, {})

    for champion_name, champion_gif in champion_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=champion_name,
                                              callback_data=f"champion_name_{champion_type_id}_{champion_name}"))
        with open(f"../pictures/{champion_gif}", 'rb') as file:
            bot.send_document(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "champion_name_" in call.data)
def champion_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Ойой, что-то не так! Перезапусти сессию, плиз!")
    else:
        champion_type_id, champion_name = int(call_data_split[2]), call_data_split[3]

        create_user_champion(call.message, champion_type_id, champion_name)

        bot.send_message(call.message.chat.id, "Добро пожаловать в ущелье призывателей!")

        game_next_step(call.message)


def create_user_champion(message, champion_type_id, champion_name):
    print(f"Начинаю создавать объекта Champion для chat id = {message.chat.id}")
    global state
    user_champion = Champion(name=champion_name,
                             champion_type=ChampionType(champion_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_champion'] = user_champion

    image_filename = champion_by_type[user_champion.champion_type][user_champion.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"../pictures/{image_filename}", 'rb') as file:
        bot.send_document(message.chat.id, file)
    bot.send_message(message.chat.id, user_champion)

    print(f"Создание объекта Champion для chat id = {message.chat.id} завершено.")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Что будешь защищать?",
                     reply_markup=body_part_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Нажми на нужный вариант на клавиатуре.")
        game_next_step(message)
    else:
        bot.send_message(message.chat.id,
                         "Куда будешь бить?",
                         reply_markup=body_part_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_body_part=message.text)


def reply_attack(message: Message, defend_body_part: str):
    if not BodyPart.has_item(message.text):
        bot.send_message(message.chat.id, "Нажми на нужный вариант на клавиатуре.")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        user_champion = state[message.chat.id]['user_champion']

        champion_npc = state[message.chat.id]['npc_champion']

        user_champion.next_step_points(next_attack=BodyPart[attack_body_part],
                                       next_defence=BodyPart[defend_body_part])

        champion_npc.next_step_points()

        game_step(message, user_champion, champion_npc)


def game_step(message: Message, user_champion: Champion, champion_npc: Champion):
    comment_npc = champion_npc.get_hit(opponent_attack_point=user_champion.attack_point,
                                       opponent_hit_power=user_champion.hit_power,
                                       opponent_type=user_champion.champion_type)
    bot.send_message(message.chat.id, f"Твой противник: {comment_npc}\nЗдоровье: {champion_npc.hp}")

    comment_user = user_champion.get_hit(opponent_attack_point=champion_npc.attack_point,
                                         opponent_hit_power=champion_npc.hit_power,
                                         opponent_type=champion_npc.champion_type)
    bot.send_message(message.chat.id, f"Твой чемпион: {comment_user}\nЗдоровье: {user_champion.hp}")

    if champion_npc.state == ChampionState.READY and user_champion.state == ChampionState.READY:
        bot.send_message(message.chat.id, "Давай еще!!")
        game_next_step(message)
    elif champion_npc.state == ChampionState.DEFEATED and user_champion.state == ChampionState.DEFEATED:
        bot.send_message(message.chat.id, "Ты сильный противник! Пока ничья!")
        update_save_stat(message.chat.id, GameResult.Ничья)
    elif champion_npc.state == ChampionState.DEFEATED:
        bot.send_message(message.chat.id, "Ну что ж, сегодня тебе повезло... Ты победил!")
        update_save_stat(message.chat.id, GameResult.Победы)
    elif champion_npc.state == ChampionState.DEFEATED:
        bot.send_message(message.chat.id, "Лузер!")
        update_save_stat(message.chat.id, GameResult.Поражения)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
