import random

from project_students.Summoners_Rift.champion import Champion
from project_students.Summoners_Rift.champion_type import ChampionType
from project_students.Summoners_Rift.champion_by_type import champion_by_type
from project_students.Summoners_Rift.body_part import BodyPart


class ChampionNPC(Champion):
    def __init__(self):
        rand_type_value = random.randint(ChampionType.min_value(), ChampionType.max_value())
        rand_champion_type = ChampionType(rand_type_value)

        rand_champion_name = random.choice(list(champion_by_type.get(rand_champion_type, {}).keys()))

        super().__init__(rand_champion_name, rand_champion_type)

    def next_step_points(self, **kwargs):
        attack_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        defence_point = BodyPart(random.randint(BodyPart.min_value(), BodyPart.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    champion_npc = ChampionNPC()
    champion_npc.next_step_points()
    print(champion_npc)
