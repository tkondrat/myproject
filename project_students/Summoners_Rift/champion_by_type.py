from project_students.Summoners_Rift.champion_type import ChampionType

champion_by_type = {

    ChampionType.Маг: {'Ари'
                       : 'Аричка.gif',
                       'Люкс': 'Люкся.gif',
                       'Физ': 'Физ.gif'},

    ChampionType.Стрелок: {'Джинкс': 'Джинкс.gif',
                           'Кайса': 'Кайса.gif',
                           'Кейт':
                               'Кейт.gif'},

    ChampionType.Танк: {'Блицкранк': 'Блиц.gif',
                        'Насус':
                            'Насус.gif',
                        'Треш': 'Треш.gif'}
}
