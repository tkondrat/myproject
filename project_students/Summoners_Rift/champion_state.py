from enum import Enum, auto


class ChampionState(Enum):
    READY = auto()
    DEFEATED = auto()
