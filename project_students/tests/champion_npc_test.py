import random

from project_students.Summoners_Rift.champion_npc import ChampionNPC
from project_students.Summoners_Rift.champion_state import ChampionState
from project_students.Summoners_Rift.champion_type import ChampionType
from project_students.Summoners_Rift.champion_types_weaknesses import champion_defence_weaknesses_by_type as \
    weaknesses_by_type


class TestChampionNPCClass:
    champion_name = 'Люкс.\nЛюкс - маг родом из страны, где к магическим способностям относятся со страхом и ' \
                    'подозрением. Умеет управлять светом, тайно использует свои силы на благо родины. Убивает ' \
                    'танков и стрелков на расстоянии.'
    champion_type = ChampionType.Маг
    max_hp = 100

    def setup_method(self, method):
        random.seed(123)

    def test_init(self):
        champion_test = ChampionNPC()

        assert champion_test.name == self.__class__.champion_name
        assert champion_test.champion_type == self.__class__.champion_type
        assert champion_test.weaknesses == weaknesses_by_type[self.__class__.champion_type]
        assert champion_test.hp == self.__class__.max_hp
        assert champion_test.attack_point is None
        assert champion_test.defence_point is None
        assert champion_test.hit_power == 30
        assert champion_test.state == ChampionState.READY

    def test_str(self):
        champion_test = ChampionNPC()
        assert str(champion_test) == f"Чемпион: {self.__class__.champion_name}\nРоль: {self.__class__.champion_type.name}\nЗдоровье: {self.__class__.max_hp}"
