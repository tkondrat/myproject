from ..Summoners_Rift.champion import Champion
from ..Summoners_Rift.champion_state import ChampionState
import project_students.Summoners_Rift.champion_type
from ..Summoners_Rift.champion_types_weaknesses import champion_defence_weaknesses_by_type as \
    weaknesses_by_type


class TestChampionClass:
    champion_name = 'Треш'
    champion_type = project_students.Summoners_Rift.champion_type.ChampionType.Танк
    max_hp = 100

    def test_init(self):
        champion_test = Champion(name=self.__class__.champion_name,
                                 champion_type=self.__class__.champion_type)

        assert champion_test.name == self.__class__.champion_name
        assert champion_test.champion_type == self.__class__.champion_type
        assert champion_test.weaknesses == weaknesses_by_type[self.__class__.champion_type]
        assert champion_test.hp == self.__class__.max_hp
        assert champion_test.attack_point is None
        assert champion_test.defence_point is None
        assert champion_test.hit_power == 30
        assert champion_test.state == ChampionState.READY

    def test_str(self):
        champion_test = Champion(name=self.__class__.champion_name,
                                 champion_type=self.__class__.champion_type)
        assert str(champion_test) == f"Имя: {self.__class__.champion_name} | " \
                                     f"Роль: {self.__class__.champion_type.name}\n" \
                                     f"Здоровье: {self.__class__.max_hp}"
