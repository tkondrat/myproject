"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(the_path: str, the_dictionary: dict):
    with open(the_path, 'wb') as pickle_file:
        pickle.dump(the_dictionary, pickle_file)


if __name__ == '__main__':
    my_dictionary = {1: 'level', 2: 'crazy', 3: '4,56'}
    my_path = 'dict.pkl'

    save_dict_to_file_pickle(my_path, my_dictionary)

    with open(my_path, 'rb') as f:
        dict_loaded = pickle.load(f)

    print(f'type: {type(dict_loaded)}')
    print(f'equal: {my_dictionary == dict_loaded}')

