"""
lambda возводит в квадрат переданный аргумент возвращает результат
"""


def fun(x):
    return x ** 2


print(fun(8))

#

alias_fun = fun

#

print(alias_fun(8))

#

lam = lambda x: x ** 2

print(lam(8))

alias_lam = lam

print(alias_lam(8))
