"""
lambda делит аргумент a на аргумент b и выводит результат
"""


def lam(a, b):
    return a / b


print(lam(6, 3))

#

print((lambda a, b: a / b)(6, 3))

#

(lambda a, b: print(a / b))(6, 3)
