"""
lambda перемножает аргументы a, b и c и выводит результат
"""


def lam(a, b, c):
    return a * b * c


print(lam(5000, 3, 2.12))

#

print((lambda a, b, c: a * b * c)(5000, 3, 2.12))

#

(lambda a, b, c: print(a * b * c))(5000, 3, 2.12)
