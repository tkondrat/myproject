"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(number1, number2, number3):
    if number1 + number2 == number3 or number2 + number3 == number1 or number1 + number3 == number2:
        return True
    else:
        return False


if __name__ == '__main__':
    number1 = int(input("Введите число1: "))
    number2 = int(input("Введите число2: "))
    number3 = int(input("Введите число3: "))

    result = check_sum(number1, number2, number3)
    print(result)
