"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""


def arithmetic(number1, number2, operation):
    if operation in "+":
        return number1 + number2
    elif operation in "-":
        return number1 - number2
    elif operation in "*":
        return number1 * number2
    elif operation in "/":
        return number1 / number2
    else:
        return str("Unknown operator")


if __name__ == '__main__':
    result = arithmetic(8, 45, '/')
    print(result)

