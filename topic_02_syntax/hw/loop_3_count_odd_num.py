"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""


def count_odd_num(num):
    if type(num) != int:
        return "Must be int!"
    elif num <= 0:
        return "Must be > 0!"
    else:
        odd_count = 0
        my_str = str(num)
        for char in my_str:
            if int(char) % 2 == 1:
                odd_count += 1
        return odd_count


if __name__ == '__main__':
    print(count_odd_num)


