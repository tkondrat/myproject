"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""

def if_3_do(number):
    if number > 3:
        number += 10
        return number
    else:
        number -= 10
    return number


if __name__ == '__main__':
    number = int (input('Введите число: '))

    result = if_3_do(number)
    print(result)


