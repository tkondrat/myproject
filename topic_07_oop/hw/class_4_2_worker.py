"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""


class Worker:
    def __init__(self, name, salary, position):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == '__main__':
    W1 = Worker("Иисус", 100000000000000, "Бог")
    W2 = Worker("Сатана", 10000000000000000, "Дьявол")
    print(W1 > W2)
    print(len(W2))

