"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
"""


class Human:

    def __init__(self, age: int, first_name: str, last_name: str):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def get_age(self):
        return self.age

    def __eq__(self, other):
        return self.age == other.age and self.first_name == other.first_name and self.last_name == other.last_name

    def __str__(self):
        return f'Имя: {self.first_name} {self.last_name} Возраст: {self.age}'


if __name__ == '__main__':
    P1 = Human(150, 'Gendalf', 'Borisovitsch')
    P2 = Human(37, 'Jesus', 'Christ')
    P3 = Human(88, 'Ivan', 'Petrov')
    print(P1.get_age())
    print(P2.get_age())
    print(P3.get_age())

    print(P1 == P2)

