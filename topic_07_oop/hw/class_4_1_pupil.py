"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""


class Pupil:

    def __init__(self, name, age, marks):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        marks_list = []
        for mark in list(self.marks.values()):
            marks_list.extend(mark)
        return marks_list

    def get_avg_mark_by_subject(self, subject):
        if subject in self.marks.keys():
            subject_list = self.marks.get(subject, [])
            return sum(subject_list) / len(subject_list)
        else:
            return 0

    def get_avg_mark(self):
        marks_list = self.get_all_marks()
        return sum(marks_list) / len(marks_list) if marks_list else 0

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == '__main__':
    Pupil1 = Pupil("Синдзи", 15, {'пилотирование Евы': [3, 5], 'синхронизация с Аской': [2, 3, 4]})
    Pupil2 = Pupil("Аянами", 9, {'Психология': [2, 4, 5], 'Кулинария': [0, 3, 2]})
    print(Pupil1.get_all_marks())
    print(Pupil1.get_avg_mark_by_subject("синхронизация с Асукой"))
    print(Pupil1.get_avg_mark())

    print(Pupil2.get_all_marks())
    print(Pupil2.get_avg_mark_by_subject("Кулинария"))
    print(Pupil2.get_avg_mark())


