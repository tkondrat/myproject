"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        all_marks = [m.get_avg_mark() for m in self.people if isinstance(m, Pupil)]
        sum_marks = sum(all_marks)
        return sum_marks / len(all_marks)

    def get_avg_salary(self):
        all_salary = [s.salary for s in self.people if isinstance(s, Worker)]
        sum_salary = sum(all_salary )
        return sum_salary / len(all_salary )

    def get_worker_count(self):
        return len([w for w in self.people if isinstance(w, Worker)])

    def get_pupil_count(self):
        return len([p for p in self.people if isinstance(p, Pupil)])

    def get_pupil_names(self):
        return [n.name for n in self.people if isinstance(n, Pupil)]

    def get_unique_worker_positions(self):
        return set([p.position for p in self.people if isinstance(p, Worker)])

    def get_max_pupil_age(self):
        return max([a.age for a in self.people if isinstance(a, Pupil)])

    def get_min_worker_salary(self):
        return min([s.salary for s in self.people if isinstance(s, Worker)])

    def get_min_salary_worker_names(self):
        return [n.name for n in self.people if isinstance(n, Worker) if n.salary == self.get_min_worker_salary()]


if __name__ == '__main__':
    Pupil1 = Pupil("Синдзи", 15, {'пилотирование Евы': [3, 5], 'синхронизация с Аской': [2, 3, 4]})
    Pupil2 = Pupil("Аянами", 9, {'Психология': [2, 4, 5], 'Кулинария': [0, 3, 2]})

    W1 = Worker("Иисус", 100000000000000, "Бог")
    W2 = Worker("Сатана", 10000000000000000, "Дьявол")

    School = School([Pupil1, Pupil2, W1, W2], 666)

    print("get_avg_mark: ", School.get_avg_mark())
    print("get_avg_salary: ", School.get_avg_salary())
    print("get_worker_count: ", School.get_worker_count())
    print("get_pupil_count: ", School.get_pupil_count())
    print("get_pupil_names: ", School.get_pupil_names())
    print("get_unique_worker_positions: ", School.get_unique_worker_positions())
    print("get_max_pupil_age: ", School.get_max_pupil_age())
    print("get_min_worker_salary: ", School.get_min_worker_salary())
    print("get_min_salary_worker_names: ", School.get_min_salary_worker_names())