"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:
    def __init__(self, name, age, milk_per_day):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other: int):
        self.milk_per_day *= other
        return self

    # проверка
    def __str__(self):
        return f'Имя: {self.name} Возраст: {self.age} Молоко в день: {self.milk_per_day}'


if __name__ == '__main__':
    goat1 = Goat("Коза", 2, 3)
    goat2 = Goat("Дереза", 4, 2)
    print(goat1)
    print(goat2)


