"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""

from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:
    def __init__(self, name, owner):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        return len([g for g in self.zoo_animals if isinstance(g, Goat)])

    def get_chicken_count(self):
        return len([ch for ch in self.zoo_animals if isinstance(ch, Chicken)])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum([g.milk_per_day for g in self.zoo_animals if isinstance(g, Goat)])

    def get_eggs_count(self):
        return sum([ch.eggs_per_day for ch in self.zoo_animals if isinstance(ch, Chicken)])


if __name__ == '__main__':
    Farm = Farm("Dogma", "Lilith")
    Farm.zoo_animals.extend([Chicken("Keter", 200, 45),
                             Chicken("Tiferet", 0, 5),
                             Chicken("Yesod", 6, 40),
                             Goat("Hesed", 300, 78),
                             Goat("Binah", 88, 8),
                             Goat("Gevorah", 1, 1)])

    print(Farm.get_goat_count())
    print(Farm.get_chicken_count())
    print(Farm.get_animals_count())
    print(Farm.get_milk_count())
    print(Farm.get_eggs_count())
